const stream = require('stream');

module.exports = class HexTransform extends stream.Transform {
    _transform(data, encoding, callback) {
        this.push(data.toString('hex') + "\n");
        callback();
    }
}
