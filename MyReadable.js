const stream = require('stream');

module.exports = class MyReadable extends stream.Readable {

    constructor(min, max) {
        super();
        this.min = min;
        this.max = max;
    }

    _read() {
        if (typeof this.min !== 'number') this.emit('error', new Error('Min is not a number'));
        if (typeof this.max !== 'number') this.emit('error', new Error('Max is not a number'));
        let random = Math.random() * (this.max-this.min) + this.min;
        this.push(random.toFixed(0).toString());
    }
}
