const stream = require('stream');

module.exports = class MyTransform extends stream.Transform {

    constructor(delay = 1000) {
        super();
        this.delay = delay;
    }

    _transform(data, encoding, callback) {
        Promise.resolve()
            .then(() => this.push("[" + data + "]"))
            .then(() => {
                return new Promise((resolve, reject) => {
                    setTimeout(() => {
                        callback();
                        resolve();
                    }, this.delay);
                });
            })
            .catch(err => {
                console.log(err);
            })
    }
}
