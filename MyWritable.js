const stream = require('stream');

module.exports = class MyWritable extends stream.Writable {
    _write(data, encoding, callback) {
        process.stdout.write(data + "\n");
        callback();
    }
}
