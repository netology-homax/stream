let MyReadable = require('./MyReadable');
let MyWritable = require('./MyWritable');
let MyTransform = require('./MyTransform');

(new MyReadable(1, 100)).pipe(new MyTransform()).pipe(new MyWritable());
