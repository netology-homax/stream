const crypto = require('crypto');
const fs = require('fs');
let HexTransform = require('./HexTransform');

const hash = crypto.createHash('md5');

let fileStream = fs.ReadStream('./node_stream.pdf');

fileStream.pipe(hash).pipe(new HexTransform()).pipe(fs.WriteStream('./output.txt'));
fileStream.pipe(hash).pipe(new HexTransform()).pipe(process.stdout);
